package com.api.drogaria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.drogaria.entities.Categoria;
import com.api.drogaria.repositories.CategoriaRepository;

@RestController
@RequestMapping("/categoria/api")
public class CategoriaController {
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@GetMapping
	public List<Categoria> findAll(){
		List<Categoria>categoriaList = categoriaRepository.findAll();
		return categoriaList;
	}

}
