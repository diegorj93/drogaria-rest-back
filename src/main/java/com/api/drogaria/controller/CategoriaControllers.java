package com.api.drogaria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.drogaria.entities.Categoria;
import com.api.drogaria.repositories.CategoriaRepositories;

@RestController
@RequestMapping("api/categoria")
public class CategoriaControllers {
	
	@Autowired
	private CategoriaRepositories categoriaRepositories;
	
	@GetMapping
	public List<Categoria>findAll(){
	 List<Categoria>categoriaList = categoriaRepositories.findAll();
	 return categoriaList;
	}

	
	
}
