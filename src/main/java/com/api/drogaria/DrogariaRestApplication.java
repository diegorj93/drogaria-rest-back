package com.api.drogaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DrogariaRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrogariaRestApplication.class, args);
	}

}
