package com.api.drogaria.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.drogaria.entities.Categoria;

@Repository
public interface CategoriaRepositories extends JpaRepository<Categoria, Long> {

}
